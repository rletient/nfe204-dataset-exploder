package org.homework.nfe204

import groovy.json.JsonOutput
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import org.homework.nfe204.domain.Agent
import org.homework.nfe204.domain.Dossier
import org.homework.nfe204.domain.Telephonie

class Exploder {

    private File inputFile
    private File outputDirectory

    private File dossierOutputFile
    private File agentOutputFile
    private File telephonieOutputFile

    private Exploder() {

    }

    private void initOutputFile() {
        dossierOutputFile = new File(outputDirectory, "dossier.txt")
        agentOutputFile = new File(outputDirectory, "agent.txt")
        telephonieOutputFile = new File(outputDirectory, "telephonie.txt")
    }

    private writeTo(Object domainObject, File file) {
        file << JsonOutput.toJson(domainObject)
        file << "\n"
    }

    private void explode() {
        final CSVParser parser = new CSVParser(new FileReader(inputFile), CSVFormat.DEFAULT.withHeader())
        parser.each {CSVRecord csvRecord ->
            Agent agent = Agent.buildFrom(csvRecord)
            Dossier dossier = Dossier.buildFrom(csvRecord)
            Telephonie telephone = Telephonie.buildFrom(csvRecord, dossier, agent)
            writeTo(agent, agentOutputFile)
            writeTo(dossier, dossierOutputFile)
            writeTo(telephone, telephonieOutputFile)
        }
    }

    static void main(String[] args) {
        def cli = new CliBuilder(usage: 'java -jar Exploder -[f]')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            f longOpt: 'file', required: true, args: 1, argName: 'file', 'Dataset STA211 en entrée'
            d longOpt: 'directory',required: true, args: 1, argName: 'directory', 'Localisation pour déposer les fichiers une fois créés'
        }
        def options = cli.parse(args)
        if (!options) {
            return
        }
        if (options.h) {
            cli.usage()
        }

        File inputFile = new File(options.f)
        File outputDir = new File(options.d)

        Exploder exploder = new Exploder()
        exploder.inputFile = inputFile
        exploder.outputDirectory = outputDir
        exploder.initOutputFile()
        exploder.explode()
    }
}
