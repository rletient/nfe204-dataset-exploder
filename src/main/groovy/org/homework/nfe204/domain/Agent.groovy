package org.homework.nfe204.domain

import groovy.json.JsonOutput
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.csv.CSVRecord

import java.security.MessageDigest

class Agent {

    // AGT_AGE,
    int age

    // AGT_SEXE,
    String sexe

    // AGT_ANCIENNETE,
    int anciennete

    // AGT_CONTRAT,
    String contrat

    // AGT_STATUT,
    String statut

    // AGT_CPT_COMPETENCE,
    int nbrCompetence

    // AGT_SITE
    String site

    String identity

    static Agent buildFrom(CSVRecord csvRecord) {
        Agent agent = new Agent()
        agent.age = Integer.parseInt(csvRecord.get("AGT_AGE"))
        agent.sexe = csvRecord.get("AGT_SEXE")
        agent.anciennete = Integer.parseInt(csvRecord.get("AGT_ANCIENNETE"))
        agent.contrat = csvRecord.get("AGT_CONTRAT")
        agent.statut = csvRecord.get("AGT_STATUT")
        agent.nbrCompetence = Integer.parseInt(csvRecord.get("AGT_CPT_COMPETENCE"))
        agent.site = csvRecord.get("AGT_SITE")
        String tmp = agent.site + agent.nbrCompetence + agent.statut + agent.contrat + agent.anciennete + agent.sexe + agent.age
        agent.identity = DigestUtils.md5Hex(tmp.getBytes())
        return agent
    }
}
