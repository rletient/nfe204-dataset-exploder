package org.homework.nfe204.domain

import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.csv.CSVRecord

class Dossier {

    // DOSSIER_PART_AGE
    int age
    // DOSSIER_PART_SEXE,
    String sexe
    // DOSSIER_PART_NB_ENFANTS,
    int nbEnfants
    // DOSSIER_PART_NB_ENFANTS_MINEURS,
    int nbEnfantsMineurs
    // DOSSIER_PART_ACTIVITE,
    String activite
    // DOSSIER_PART_CATEGORIE,
    String categorie
    // DOSSIER_PART_TOP_INA_NON_TRAITE,
    String topInaNonTraite
    // DOSSIER_PART_FLAG_ADH_FUA,
    String flagAdhFua
    // DOSSIER_PART_FLAG_ADH_FUR,
    String flagAdhFur
    // DOSSIER_PART_FLAG_ADH_FAR,
    String flagAdhFar
    // DOSSIER_PART_FLAG_ADH_AUTRES_FM,
    String flagAdhAutresFM
    // DOSSIER_PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE,
    String flagAdhFMSurComplementaire
    // DOSSIER_PART_REGION
    String region

    String identity


    static Dossier buildFrom(CSVRecord csvRecord) {
        Dossier dossier = new Dossier()
        dossier.age = Integer.parseInt(csvRecord.get("DOSSIER_PART_AGE"))
        dossier.sexe = csvRecord.get("DOSSIER_PART_SEXE")
        dossier.nbEnfants = Integer.parseInt(csvRecord.get("DOSSIER_PART_NB_ENFANTS"))
        dossier.nbEnfantsMineurs = Integer.parseInt(csvRecord.get("DOSSIER_PART_NB_ENFANTS_MINEURS"))
        dossier.activite = csvRecord.get("DOSSIER_PART_ACTIVITE")
        dossier.categorie = csvRecord.get("DOSSIER_PART_CATEGORIE")
        dossier.topInaNonTraite = csvRecord.get("DOSSIER_PART_TOP_INA_NON_TRAITE")
        dossier.flagAdhFua = csvRecord.get("DOSSIER_PART_FLAG_ADH_FUA")
        dossier.flagAdhFur = csvRecord.get("DOSSIER_PART_FLAG_ADH_FUR")
        dossier.flagAdhFar = csvRecord.get("DOSSIER_PART_FLAG_ADH_FAR")
        dossier.flagAdhAutresFM = csvRecord.get("DOSSIER_PART_FLAG_ADH_AUTRES_FM")
        dossier.flagAdhFMSurComplementaire = csvRecord.get("DOSSIER_PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE")
        dossier.region = csvRecord.get("DOSSIER_PART_REGION")

        String tmp = dossier.age + dossier.sexe + dossier.nbEnfants + dossier.nbEnfantsMineurs + dossier.activite +
                dossier.categorie + dossier.topInaNonTraite + dossier.flagAdhFua + dossier.flagAdhFur + dossier.flagAdhFar +
                 dossier.flagAdhAutresFM + dossier.flagAdhFMSurComplementaire + dossier.region
        dossier.identity = DigestUtils.md5Hex(tmp.getBytes())
        return dossier
    }
}
