package org.homework.nfe204.domain

import org.apache.commons.csv.CSVRecord

class Telephonie {

    // TEL_HEURE,
    int heure
    // TEL_JOUR_SEMAINE,
    int jourSemaine
    // TEL_TALKTIME,
    int talkTime
    // TEL_QUEUETIME
    int queueTime
    // TEL_CLASSE_METIER
    String classeMetier

    String agent

    String dossier

    static Telephonie buildFrom(CSVRecord csvRecord, Dossier dossier, Agent agent) {
        Telephonie telephonie = new Telephonie()
        telephonie.heure = Integer.parseInt(csvRecord.get("TEL_HEURE"))
        telephonie.jourSemaine = Integer.parseInt(csvRecord.get("TEL_JOUR_SEMAINE"))
        telephonie.talkTime = Integer.parseInt(csvRecord.get("TEL_TALKTIME"))
        telephonie.queueTime = Integer.parseInt(csvRecord.get("TEL_QUEUETIME"))
        telephonie.classeMetier = csvRecord.get("TEL_CLASSE_METIER")
        telephonie.agent = agent.identity
        telephonie.dossier = dossier.identity
        return telephonie
    }
}
